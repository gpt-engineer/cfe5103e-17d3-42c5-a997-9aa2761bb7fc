document.getElementById('task-form').addEventListener('submit', function(event) {
  // Prevent the form from submitting
  event.preventDefault();

  // Get the task input element
  var taskInput = document.getElementById('task-input');

  // Create a new list item
  var li = document.createElement('li');
  li.textContent = taskInput.value;
  li.className = 'border mb-4 p-2';

  // Create a delete button
  var deleteButton = document.createElement('button');
  deleteButton.textContent = 'Delete';
  deleteButton.className = 'bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded focus:outline-none focus:shadow-outline float-right';
  deleteButton.addEventListener('click', function() {
    // Remove the parent list item from the DOM
    this.parentNode.remove();
  });

  // Add the delete button to the list item
  li.appendChild(deleteButton);

  // Add the list item to the task list
  document.getElementById('task-list').appendChild(li);

  // Clear the task input
  taskInput.value = '';
});
